
FILE = "modules.csv"

class Module():
    def __init__(self, name, year, credits, percent):
        self._name = name
        self._credits = credits
        self._percent = percent
        self._year = year

    def get_name(self):
        return self._name

    def get_year(self):
        return self._year

    def get_credits(self):
        return self._credits

    def get_percent(self):
        return self._percent

    def set_percent(self, val):
        self._percent = val

    def __str__(self):
        return "Year %d: %s (%d credits, %d%%)" % (self._year, self._name, self._credits, self._percent)

class ModuleBundle():
    def __init__(self, weight, num_credits):
        self._modules = []
        self._weight = weight
        self._credit_capacity = num_credits

    def get_percent(self):
        return sum([module.get_percent() for module in self._modules]) / len(self._modules)

    def add(self, module):
        if self.can_add(module):
            self._modules.append(module)
        else:
            raise Exception("No Space in module bundle for %s" % module)

    def get_weight(self):
        return self._weight

    def can_add(self, module=None):
        credits = module.get_credits() if module else 0
        return self._get_total_credits() < self._credit_capacity + credits

    def _get_total_credits(self):
        return sum([module.get_credits() for module in self._modules])

    def __str__(self):
        detail = "Max Credits: %d, Multiplier %d\n" % (self._credit_capacity, self._weight)
        for module in self._modules:
            detail += "\t" + str(module) + "\n"
        return detail


def bundle(modules):
    modules.sort(key=lambda m: m.get_percent(), reverse=True)
    year_2_modules = [module for module in modules if module.get_year() == 2]
    year_3_modules = [module for module in modules if module.get_year() == 3]

    year_3_bundle = ModuleBundle(3/6, 80)
    mid_bundle = ModuleBundle(2/6, 80)
    year_2_bundle = ModuleBundle(1/6, 80)

    year_3_pool = [module for module in year_3_modules]
    bundle_pack(year_3_bundle, year_3_pool)

    mid_pool = year_3_pool + year_2_modules
    bundle_pack(mid_bundle, mid_pool)

    year_2_pool = mid_pool
    assert len([module for module in year_2_pool if module.get_year() > 2]) == 0
    bundle_pack(year_2_bundle, year_2_pool)

    return [year_3_bundle, mid_bundle, year_2_bundle]


def bundle_pack(bundle, module_pool):
    while bundle.can_add() and len(module_pool) != 0 :
        module = module_pool[0]
        if bundle.can_add(module):
            bundle.add(module)
            module_pool.remove(module)


def print_modules(modules):
    for module in modules:
        print(module)

def read_csv(file_name):
    modules = []
    with open(file_name) as f:
        for line in f.readlines():
            line = line.strip()
            parts = line.split(',')
            name, year, credits, percent = parts[0], int(parts[1]), int(parts[2]), int(parts[3])
            modules.append(Module(name,year,credits, percent))
    return modules


def weighted_average(bundles):
    return sum([bundle.get_percent() * bundle.get_weight() for bundle in bundles])

def validate_modules(modules):
    num_credits_year_2 = sum(module.get_credits() for module in modules if module.get_year() == 2)
    num_credits_year_3 = sum(module.get_credits() for module in modules if module.get_year() == 3)
    validation = {
        "Third year 120 credits": num_credits_year_3 == 120,
        "Second year 120 credits": num_credits_year_2 == 120,
        "Valid percents": len([m for m in modules if m.get_percent() < 0 or m.get_percent() > 100]) == 0,
    }
    valid = all(validation.values())
    return valid, validation


if __name__ == '__main__':
    modules = read_csv(FILE)
    valid, reason = validate_modules(modules)
    if not valid:
        print("Error: invalid 'modules.csv' file\n\t" + str(reason))
        exit(1)

    bundles = bundle(modules)
    for b in bundles:
        print(b)
    print("Weighted Average: %d%%" % weighted_average(bundles))
