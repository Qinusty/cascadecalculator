# Cascade Calculator
This is a cascade calculator for Aberystwyth University, it works by taking a weighted average of modules.
**note**: This currently only works with three year degrees (Year in Industry grade is not counted, masters is not currently supported)


The top 80 credits of your final year has a weight of 3.
The bottom 40 credits of your third year, and top 40 credits of second year have a weight of 2.
The remaining 80 credits of your second year have a weight of 1

## Instructions for use
### `module.csv`
This file is used to tell the program which modules you're doing, how many credits they're worth and what percent you got or expect to get in them.

The current file should be structured as follows per row.
`name,year,credits,percent` where
- name can be anything which helps you identify it.
- year shall be 2 or 3
- credits is the number of credits the module is worth.
- percent is the percent you got on the module as a whole. (Or expect to get)

### Easy
~~TODO~~

### Python
Python 3 required, tested on Python 3.6
Run `visual.py` for a visual representation with sliders for tweaking grades.
Run `calculate.py` for a simple output based on your module.csv file

## Disclaimer
This is not guaranteed to be accurate, although efforts have been made to ensure the accuracy you should still rely on the student record cascade calculator for accurate calculations.
