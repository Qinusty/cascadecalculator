from tkinter import *
from tkinter import messagebox

from calculate import read_csv, weighted_average, bundle, validate_modules

FILE = "modules.csv"

models = []
output_label = None

class ResultModel():
    def __init__(self, module):
        self.var = IntVar()
        self.module = module
        self.var.set(module.get_percent())

    def update(self):
        self.module.set_percent(self.var.get())

def calc():
    for model in models:
        model.update()
    bundles = bundle([model.module for model in models])
    output_label.config(text = "Grade: " + str(round(weighted_average(bundles))))



if __name__ == '__main__':
    root = Tk()
    root.geometry("500x800")
    modules = read_csv(FILE)
    valid, reason = validate_modules(modules)
    if not valid:
        messagebox.showerror("Error", "Error: Invalid 'modules.csv' file\n\t" + str(reason))
        exit(1)

    for module in modules:
        rm = ResultModel(module)
        models.append(rm)
        scale = Scale( root, variable = rm.var , from_=0, to_=100, orient=HORIZONTAL, command=lambda _: calc(), label=rm.module.get_name())
        scale.pack(anchor = CENTER)

    button = Button(root, text = "Calculate", command = calc)
    button.pack(anchor = CENTER)

    output_label = Label(root)
    output_label.pack()

    root.mainloop()
